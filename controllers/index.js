var mysql      = require('mysql');
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'alias_game'
});
connection.connect();

exports.index = function(req, res){
  res.render('index', { title: 'alias.game' })
};

exports.dictionary = function(req, res){
    connection.query('SELECT word FROM `dictionary2` ORDER BY RAND() LIMIT 1000', function(err, rows, fields) {
        if (err) throw err;
       // connection.end();
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({ words: rows }));
    });

};