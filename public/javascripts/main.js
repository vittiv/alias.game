var app = angular.module('alias.game', ['ngResource']);

function Team(param){
    this.name = param.name;
    this.member_one = param.member_one;
    this.member_two = param.member_two;
    this.guesses = 0;
    this.pass = [];
    this.show_details = false;
}

app.factory('dictionary', ['$resource',
    function($resource){
        return $resource('/dictionary', {}, {
            query: {method:'GET'}
        });
    }]);

app.controller('SpicyController', ['$scope', '$interval', 'dictionary', function($scope, $interval, dictionary ) {

    $scope.render = function(name){
        $scope.page = '/html/' + name + '.html';
    };

    $scope.load_words = function(){
        dictionary.get(function(data) {
            $scope.current_word = 0;
            $scope.words = data.words;
        });
    };

    var init = function(){
        $scope.circle = 0;
        $scope.current_team = 0;
        $scope.teams = [];
        $scope.team_data = {};
        $scope.load_words();
        $scope.render('team_add');
    };
    init();

    $scope.addTeam = function(){
        $scope.teams.push(new Team($scope.team_data));
        $scope.team_data = {};
    };

    $scope.tic = function(){
       if($scope.remaining_time == 0){
           $interval.cancel($scope.stopTime);
           $scope.render('next_team');
       }
       $scope.remaining_time--;
    };

    $scope.ready = function(){
        $scope.remaining_time = 20;
        $scope.stopTime = $interval($scope.tic, 1000);
        $scope.word_check();
        $scope.render('game');
    };

    /**
     *
     * @returns {boolean}
     */
    $scope.is_circle_end = function(){
        return $scope.current_team == ($scope.teams.length-1);
    };

    $scope.next_team = function(){
        if($scope.is_circle_end()){
            $scope.current_team = 0;
            $scope.circle++;
        }else{
            $scope.current_team++;
        }
        $scope.render('ready');
    };

    /**
     *
     * @returns {*}
     */
    $scope.get_team = function(){
        return $scope.teams[$scope.current_team];
    };

    /**
     *
     * @returns {*}
     */
    $scope.get_word = function(){
        return $scope.words[$scope.current_word].word;
    };

    $scope.guessing = function(){
        $scope.teams[$scope.current_team].guesses++;
        $scope.word_check();
    };

    $scope.pass = function(word){
        $scope.teams[$scope.current_team].pass.push(word);
        $scope.word_check();
    };

    $scope.word_check = function(){
        if ($scope.current_word == ($scope.words.length-1)){
          $scope.load_words();
        }else{
          $scope.current_word++;
        }
    };

}]);